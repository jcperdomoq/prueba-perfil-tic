<?php

namespace App\Http\Controllers\Business;

use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     *constructor.
     */
    public function __construct(){
        $this->middleware(['jwt'])->except(['index','show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        return new CategoryResource(Category::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new CategoryResource(Category::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string',
            'parent_id' => 'nullable|exists:categories,id',
        ]);

        $category=Category::findOrFail($id);
        $category->fill($request->all());
        $category->save();

        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::findOrFail($id);
        Storage::disk('public')->delete($category->photo);
        $category->delete();
        return new CategoryResource($category);
    }

    public function addImagen(Request $request,$id){
        $request->validate([
            'photo' => 'required|image',
        ]);
        $category=Category::findOrFail($id);
        if($category->photo){
            Storage::disk('public')->delete($category->photo);
        }
        $category->photo = $request->photo->store('categories', 'public');
        $category->save();
        return new CategoryResource($category);
    }
}
